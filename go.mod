module se01.com

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/golangcollege/sessions v1.2.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/text v0.3.4 // indirect
)
