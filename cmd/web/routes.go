package main

import (
	"github.com/go-chi/chi"
	"net/http"
)

func (app *application) routes() http.Handler {
	//standardMiddleware := alice.New(app.recoverPanic, app.logRequest, secureHeaders)

	mux := chi.NewRouter()
	standardGroup := mux.Group(nil)
	standardGroup.Use(app.recoverPanic, app.logRequest, secureHeaders)
	dynamicGroup := mux.Group(nil)
	dynamicGroup.Use(app.session.Enable)

	dynamicGroup.Get("/", app.home)
	dynamicGroup.Get("/snippet/create", app.createSnippetForm)
	dynamicGroup.Post("/snippet/create", app.createSnippet)
	dynamicGroup.Get("/snippet/{id:[0-9]+}", app.showSnippet)

	fileServer := http.FileServer(http.Dir("./ui/static/"))
	standardGroup.Handle("/static/", http.StripPrefix("/static", fileServer))
	return mux
	//return standardMiddleware.Then(mux)
}
